# HackerNewsReader

```
auraham@rocket ~/git/hn_elixir 
 > mix phx.new . --app hacker_news_reader --module HackerNewsReader --no-gettext --no-dashboard --no-mailer
The directory /home/auraham/git/hn_elixir already exists. Are you sure you want to continue? [Yn] y
* creating lib/hacker_news_reader/application.ex
* creating lib/hacker_news_reader.ex
* creating lib/hacker_news_reader_web/controllers/error_json.ex
* creating lib/hacker_news_reader_web/endpoint.ex
* creating lib/hacker_news_reader_web/router.ex
* creating lib/hacker_news_reader_web/telemetry.ex
* creating lib/hacker_news_reader_web.ex
* creating mix.exs
* creating README.md
* creating .formatter.exs
.formatter.exs already exists, overwrite? [Yn] y
* creating .gitignore
.gitignore already exists, overwrite? [Yn] n
* creating test/support/conn_case.ex
* creating test/test_helper.exs
* creating test/hacker_news_reader_web/controllers/error_json_test.exs
* creating lib/hacker_news_reader/repo.ex
* creating priv/repo/migrations/.formatter.exs
* creating priv/repo/seeds.exs
* creating test/support/data_case.ex
* creating lib/hacker_news_reader_web/controllers/error_html.ex
* creating test/hacker_news_reader_web/controllers/error_html_test.exs
* creating lib/hacker_news_reader_web/components/core_components.ex
* creating lib/hacker_news_reader_web/controllers/page_controller.ex
* creating lib/hacker_news_reader_web/controllers/page_html.ex
* creating lib/hacker_news_reader_web/controllers/page_html/home.html.heex
* creating test/hacker_news_reader_web/controllers/page_controller_test.exs
* creating lib/hacker_news_reader_web/components/layouts/root.html.heex
* creating lib/hacker_news_reader_web/components/layouts/app.html.heex
* creating lib/hacker_news_reader_web/components/layouts.ex
* creating priv/static/images/logo.svg
* creating priv/static/robots.txt
* creating priv/static/favicon.ico
* creating assets/js/app.js
* creating assets/vendor/topbar.js
* creating assets/css/app.css
* creating assets/tailwind.config.js

Fetch and install dependencies? [Yn] 
* running mix deps.get
* running mix assets.setup
* running mix deps.compile

We are almost there! The following steps are missing:

    $ cd .

Then configure your database in config/dev.exs and run:

    $ mix ecto.create

Start your Phoenix app with:

    $ mix phx.server

You can also run your app inside IEx (Interactive Elixir) as:

    $ iex -S mix phx.server
```





To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
