defmodule HackerNewsReader.Repo do
  use Ecto.Repo,
    otp_app: :hacker_news_reader,
    adapter: Ecto.Adapters.Postgres
end
