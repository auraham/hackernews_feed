# HackerNews

```elixir
Mix.install([
  {:httpoison, "~> 2.2.1"},
  {:floki, "~> 0.35.2"}
])
```

## Getting posts

```elixir
# Get page
url = "https://news.ycombinator.com/"
{:ok, response} = HTTPoison.get(url)
```

```elixir
# Inspect response
IEx.Helpers.i(response)
```

```elixir
is_map(response)
```

```elixir
is_struct(response)
```

```elixir
# Notice the special key :__struct__
Map.keys(response)
```

```elixir
# Get body
response.body
```

```elixir
# Parse document
{:ok, doc} =
  response.body
  |> Floki.parse_document()
```

```elixir
# Get all links
doc |> Floki.find("a")
```

```elixir
# Last link: {tag, attributes, children_nodes}
doc
|> Floki.find("a")
|> List.last()
```

```elixir
links =
  doc
  |> Floki.find("a[href^=item]")
```

```elixir
# Get href values
hrefs =
  links
  |> Enum.map(fn {_tag, [{"href", href}], _child} -> href end)
  |> Enum.uniq()
```

```elixir
length(hrefs)
```

```elixir
url = "https://news.ycombinator.com/item?id=38887531"
{:ok, response} = HTTPoison.get(url)
{:ok, doc} = response.body |> Floki.parse_document()
```

We want to extract the link of the post

```html
<span class="titleline">
  <a href="https://nitro.jan.ai/">
    Nitro: A fast, lightweight inference server with OpenAI-Compatible API
  </a>
  <span class="sitebit comhead">
    (<a href="from?site=jan.ai"><span class="sitestr">jan.ai</span></a>)
  </span>
</span>
```

To do so, we can use CSS classes.
Note that the link `<a href="https://nitro.jan.ai/">..</a>` is inside `<span class="titleline">..</span>`. Thus, we can use `span.titleline > a` to extract the link from the block:

```elixir
link_to_post =
  doc
  |> Floki.find("span.titleline > a")
```

```elixir

```

```elixir
<span class="titleline"><a href="https://nitro.jan.ai/">Nitro: A fast, lightweight inference server with OpenAI-Compatible API</a><span class="sitebit comhead"> (<a href="from?site=jan.ai"><span class="sitestr">jan.ai</span></a>)</span></span>
```

```elixir

```

```elixir
def get_locations() do
  case HTTPoison.get(@url) do
    {:ok, response} ->
      case response.status_code do
        200 ->
          locations =
            response.body
            # items = doc |> Floki.find("span.titleline") |> Floki.find("a[href^=http]")
            |> Floki.find("span.titleline")
            |> Floki.find("a[href^=http]")

          # |> Floki.find(".location-card")
          # |> Enum.map(&extract_location_name_and_id/1)
          # |> Enum.sort(&(&1.name < &2.name))
          {:ok, locations}

        code ->
          {:error, code}
      end

    _ ->
      :general_error
  end
end
```

## Spawning processes

The following block gets the list of URLs to posts in the frontpage:

```elixir
url = "https://news.ycombinator.com/"

{:ok, response} = HTTPoison.get(url)
{:ok, doc} = response.body |> Floki.parse_document()

post_urls =
  doc
  # use ^=, not =^, it leads to an error
  |> Floki.find("a[href^=item]")
  |> Enum.map(fn {_tag, [{"href", href}], _child} -> href end)
  |> Enum.uniq()
  # TODO: Use URI.new() 
  |> Enum.map(fn href -> url <> href end)
```

```elixir
post_urls
```

Now that we have a list of URLs, let's discuss how to process them concurrently.

```elixir
parse_post_url = fn url ->
  # logic to get {article_title, article_url}
  :timer.sleep(1000)
  result = {:article_title, :article_url, url}
  IO.inspect(result)
end

post_urls
|> List.first()
|> parse_post_url.()
```

```elixir
# Sequential approach
# This will take ~30 seconds
post_urls
|> Enum.map(fn url -> parse_post_url.(url) end)
```

```elixir
async_parse_post_url = fn url ->
  spawn(fn -> parse_post_url.(url) end)
end

post_urls
|> Enum.map(fn url -> async_parse_post_url.(url) end)
```

The following approach allows us to get the result from each process:

```elixir
async_parse_post_url = fn url ->
  # main process (iex)
  from = self()
  # Here we create a process that sends
  spawn(fn ->
    # a message to `from` with the results when `parse_post_url.()` is finished
    send(from, {:ok_async_post_url, parse_post_url.(url)})
  end)
end

get_results = fn ->
  receive do
    {:ok_async_post_url, result} -> result
  after
    5_000 -> {:killed, :no_result}
  end
end

# Now this takes ~1 second
results =
  post_urls
  |> Enum.map(fn url -> async_parse_post_url.(url) end)
  |> Enum.map(fn _ -> get_results.() end)
```

```elixir
url =
  post_urls
  |> List.first()

task = Task.async(fn -> parse_post_url.(url) end)
```

```elixir
Task.await(task)
```

```elixir
# more idiomatic
post_urls
|> Enum.map(fn url -> Task.async(fn -> parse_post_url.(url) end) end)
|> Enum.map(fn task -> Task.await(task, 5_000) end)
```

```elixir
# con streams, genial
# revisa las opciones adicionales, como timeout y numero de cores
post_urls
|> Task.async_stream(fn url -> parse_post_url.(url) end)
|> Enum.to_list()
```

```elixir
Task.async(fn -> parse_post_url.(url) end)
```

```elixir
post_urls
|> Enum.map(fn url -> Task.async(fn -> parse_post_url.(url) end) end)
|> Enum.map(fn url -> Task.async(fn -> parse_post_url.(url) end) end)
```

```elixir
## post_urls = 
##  |> Enum.map(fn url -> url end)
# |> Enum.map(fn task -> Task.await(task, 5_000))
```

## Gotchas

<!-- livebook:{"force_markdown":true} -->

```elixir
big_list
|> Stream.map(&Task.async(Module, :do_something, [&1]))
|> Stream.map(&Task.await(&1))
|> Enum.filter filter_fun
```

```elixir
do_work = fn item ->
  Process.sleep(2_000)
  IO.inspect("Item #{item} done")
end
```

```elixir
# This takes 10s
1..5
|> Enum.map(fn i -> do_work.(i) end)
```

```elixir
# This takes 2s
1..5
|> Enum.map(fn i -> Task.async(fn -> do_work.(i) end) end)
|> Enum.map(fn t -> Task.await(t) end)
```

```elixir
# This takes 10s, it is not concurrent
1..5
|> Stream.map(fn i -> Task.async(fn -> do_work.(i) end) end)
|> Stream.map(fn t -> Task.await(t) end)
|> Enum.to_list()
```

```elixir
# This takes 2s, but is a bit cumbersome
1..5
|> Enum.map(fn i -> Task.async(fn -> do_work.(i) end) end)
|> Stream.map(fn t -> Task.await(t) end)
|> Enum.to_list()
```

```elixir
# This takes 10s
1..5
|> Stream.map(fn i -> Task.async(fn -> do_work.(i) end) end)
|> Enum.map(fn t -> Task.await(t) end)

# |> Enum.to_list()
```

## References

* [How do you check for the type of variable in Elixir](https://stackoverflow.com/questions/28377135/how-do-you-check-for-the-type-of-variable-in-elixir)

```elixir

```

```elixir

```
